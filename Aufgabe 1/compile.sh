#!/bin/bash

source="$(find src/*.cpp | tr '\n' ' ')"
extsource="$(find extlib/poly2tri/poly2tri/common/*.cc extlib/poly2tri/poly2tri/sweep/*.cc | tr '\n' ' ')"
link="-I include/ -I extlib/poly2tri -lX11 -lpthread -lpolyclipping -lBox2D"
defines=""

function debug
{
    mkdir -p bin
    mkdir -p bin/Debug
    g++ -ggdb -std=c++14 $defines $source $extsource $link
    mv a.out bin/Debug/a1
}

function release
{
    mkdir -p bin
    mkdir -p bin/Release
    g++ -O2 -s -std=c++14 $defines $source $extsource $link
    mv a.out bin/Release/a1
}

dr=$1
if [ -z "$dr" ] ; then
    read -p "Compile for [Dd]ebug or [Rr]elease? (q to quit) : " dr
fi
while true; do
    case $dr in
    [Dd]* ) debug; break;;
    [Rr]* ) release; break;;
     [q]* ) exit;;
        * ) echo "Please provide an valid answer, either d or r!";;
    esac
    read -p "Compile for [Dd]ebug or [Rr]elease? (q to quit) : " dr
done
