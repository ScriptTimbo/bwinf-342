/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "LineSegment.hpp"
#include "Polygon.hpp"
#include "Circle.hpp"

#include <string>

namespace cimg_library
{
    template<typename T>
    class CImg;
}

class Exercise
{
public:
    friend class PhysicsBasedAlgorithm;
public:
    explicit Exercise() noexcept;
    explicit Exercise(const Polygon& pol,
                      const PointList& poi) noexcept;
    explicit Exercise(const PointList& pol,
                      const PointList& poi) noexcept;

    float length(const Point& point) const noexcept;
    float distance(const Point& point, const LineSegment& line) const noexcept;
    void minRadiusOfPoints() noexcept;
    void minRadiusPoly() noexcept;
    void findRadii() noexcept;

    void visualizeCircles(std::size_t width, std::size_t height,
                          std::string title) const noexcept;
private:
    void drawPolygon(cimg_library::CImg<unsigned char>& img,
                     float scaleX, float scaleY,
                     float toAddX, float toAddY) const noexcept;
    void drawPoints(cimg_library::CImg<unsigned char>& img,
                    float scaleX, float scaleY,
                    float toAddX, float toAddY) const noexcept;
protected:
    PointList points;
    std::vector<Circle> circles;
    Polygon polygon;
};
