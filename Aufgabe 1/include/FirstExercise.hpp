/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "Exercise.hpp"

#include <istream>
#include <ostream>

class FirstExercise : public Exercise
{
public:
    explicit FirstExercise() noexcept;
    explicit FirstExercise(const Polygon& pol, const PointList& poi) noexcept;

    friend std::ostream& operator<<(std::ostream& os, FirstExercise& ex);
    friend std::istream& operator>>(std::istream& is, FirstExercise& ex);
    void visualize(std::size_t width, std::size_t height) const noexcept;
    float ratio() noexcept;
};
