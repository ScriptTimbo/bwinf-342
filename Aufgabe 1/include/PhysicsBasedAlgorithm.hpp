/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "LineSegment.hpp"
#include "Polygon.hpp"

#include <Box2D/Box2D.h>

namespace p2t
{
    class CDT;
    class Triangle;
    class Point;
}

class PhysicsBasedAlgorithm
{
public:
    static bool asap;
    static bool unsafe;

    static float magneticForceFactor;
    static float coulombFactor;
private:
    class ContactListener : public b2ContactListener
    {
        void BeginContact(b2Contact* contact) override
        {  }
        void EndContact(b2Contact* contact) override
        {
            auto bodya = contact->GetFixtureA()->GetBody();
            if(bodya->GetType() == b2_dynamicBody)
            {
                bodya->SetLinearVelocity(b2Vec2(0.f, 0.f));
                bodya->SetAwake(false);
            }
            auto bodyb = contact->GetFixtureB()->GetBody();
            if(bodyb->GetType() == b2_dynamicBody)
            {
                bodyb->SetLinearVelocity(b2Vec2(0.f, 0.f));
                bodyb->SetAwake(false);
            }
        }
    };
public:
    struct TriangulizationImpl
    {
    public:
        TriangulizationImpl(const Polygon& poly) noexcept;
        ~TriangulizationImpl() noexcept;

        operator std::vector<p2t::Triangle*>&() noexcept;
    private:
        LineSegment aabbPoly(const Polygon& polygon) const noexcept;
    private:
        p2t::CDT* cdt;
        std::vector<p2t::Triangle*> triangles;
        std::vector<p2t::Point*> polyline;
        std::vector<p2t::Point*> hole;
    };
public:
    explicit PhysicsBasedAlgorithm(const Polygon& poly,
                                   const PointList& initial) noexcept;

    void setAttractionToBaryCenter(bool val) noexcept;
    PointList evaluate() noexcept;
private:
    void letAttractToBarycenter() noexcept;
    void simulateCoulombForce() noexcept;
    void initWorld(b2World& world, float radius) noexcept;
    void worldToPoints(b2World& world) noexcept;
    bool isEveryoneAsleep(b2World& world) const noexcept;
private:
    const Polygon& polygon;
    PointList points;
    float radius;

    TriangulizationImpl impl;
    std::vector<p2t::Triangle*> triangulizedPoly;
};
