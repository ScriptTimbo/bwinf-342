/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "LineSegment.hpp"

class Polygon
{
public:
    explicit Polygon() noexcept = default;
    explicit Polygon(const PointList& verts) noexcept;

    const float polyArea() const noexcept;
    const Point barycenterOfPolygon() const noexcept;

    const Point get(std::size_t i) const noexcept;
    void set(std::size_t i, const Point& p) noexcept;
    Point& operator[](std::size_t i) noexcept;
    const Point& operator[](std::size_t i) const noexcept;

    PointList::iterator begin() noexcept;
    PointList::const_iterator begin() const noexcept;

    PointList::iterator end() noexcept;
    PointList::const_iterator end() const noexcept;

    const Point front() const noexcept;
    const Point back() const noexcept;

    const std::size_t size() const noexcept;
    const bool empty() const noexcept;

    const LineSegment aabb() const noexcept;
public:
    PointList vertices;
};
