/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "Exercise.hpp"

#include <istream>
#include <ostream>
#include <random>
#include <queue>
#include <map>

class Random;

class SecondExercise : Exercise
{
public:
    static std::size_t pointCount;
public:
    friend std::ostream& operator<<(std::ostream& os, SecondExercise& ex);
    friend std::istream& operator>>(std::istream& is, SecondExercise& ex);

    void setLuisDistanceToCake(float dist) noexcept;
    void setLuisDistanceToCandle(float dist) noexcept;
    void visualize(std::size_t width, std::size_t height) noexcept;
private:
    float luisRatio() noexcept;
    float lindaRatio() noexcept;
    float diameter() const noexcept;

    void resizePoly(Polygon& newPoly, float dist);
    bool doCirclesIntersect() const noexcept;
    bool addLuisCircles(std::queue<Point>& luisbuf,
                        Polygon& curpoly, PointList& toInspect) noexcept;
    PointList getRandomPoints(Random& rnd) const noexcept;
    Point randomPointInPolygon(std::mt19937& mersenneTwister,
              const std::vector<Polygon>& triangulizedPolygon) const noexcept;
    std::map<float, std::size_t> triangleIndizes(
              const std::vector<Polygon>& triangulizedPolygon) const noexcept;
private:
    float luisDistanceToCake;
    float luisDistanceToCandle;
    PointList luispoints;

    bool pngOutput;
};
