/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "Point.hpp"
#include "Polygon.hpp"

class Triangulation
{
public:
    explicit Triangulation(const Polygon& polygon) noexcept;

	operator std::vector<Polygon>() noexcept;
    static bool inTriangle(Point A, Point B, Point C, Point P) noexcept;
private:
    static bool process(const Polygon& poly,
                        std::vector<Polygon>& triangulizedPoly) noexcept;
    static bool snip(const Polygon& poly,
            std::size_t u, std::size_t v, std::size_t w,
            std::size_t nv, std::vector<std::size_t>& V) noexcept;
private:
    const Polygon& poly;
};
