/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Application.hpp"
#include "PhysicsBasedAlgorithm.hpp"
#include "SecondExercise.hpp"
#include "Constants.hpp"

#include <iostream>
#include <fstream>
#include <regex>

Application::Application(const std::vector<std::string>& arguments) noexcept
            : secondExercise(false), goOn(true), graphical(false),
              heartPointCount(61U), width(600U), height(600U), ex1(), ex2(),
              heartScale(10.f)
{
    readArguments(arguments);
}

int Application::run() noexcept
{
    if(!goOn)
        return 0U;

    if(secondExercise)
        std::cout << ex2 << std::endl;
    else
        std::cout << ex1 << std::endl;

    if(graphical)
    {
        if(secondExercise)
            ex2.visualize(width, height);
        else
            ex1.visualize(width, height);
    }

    return 0U;
}

void Application::readArguments(const std::vector<std::string>& args)
{
    bool heart = false;
    std::string filename;
    bool loadFile = false;
    for(auto it = args.begin(); it != args.end(); ++it)
    {
        const auto arg = *it;

        if(arg == "-h" || arg == "--help")
        {
            printHelp();
            goOn = false;
        }
        else if(arg == "-s" || arg == "--second")
        {
            secondExercise = true;

            it = std::next(it);
            if(it == args.end())
                throw std::invalid_argument("No point count was given.");
            SecondExercise::pointCount = std::stoi(*it);

            it = std::next(it);
            if(it == args.end())
                throw std::invalid_argument("The distance 1 for Luis must be given.");
            ex2.setLuisDistanceToCake(std::stof(*it));

            it = std::next(it);
            if(it == args.end())
                throw std::invalid_argument("The distance 2 for Luis must be given.");
            ex2.setLuisDistanceToCandle(std::stof(*it));
        }
        else if(arg == "-hs" || arg == "--heart-scale")
        {
            it = std::next(it);
            if(it == args.end())
                throw std::invalid_argument("No heart scale was given.");
            heartScale = std::stof(*it);

            if(heartScale < 2.f)
                throw std::invalid_argument("The scale factor really shouldn\'t go lower than 2.");
        }
        else if(arg == "-cf" || arg == "--coulomb-factor")
        {
            it = std::next(it);
            if(it == args.end())
                throw std::invalid_argument("No coulomb factor was given.");
            PhysicsBasedAlgorithm::coulombFactor = std::stof(*it);
        }
        else if(arg == "-mf" || arg == "--magnetic-force-factor")
        {
            it = std::next(it);
            if(it == args.end())
                throw std::invalid_argument("No magnetic force factor was given.");
            PhysicsBasedAlgorithm::magneticForceFactor = std::stof(*it);
        }
        else if(arg == "-f" || arg == "--file")
        {
            it = std::next(it);
            if(it == args.end())
                throw std::invalid_argument("No file name was given.");

            loadFile = true;
            filename = *it;
        }
        else if(arg == "-he" || arg == "--heart")
        {
            heart = true;
        }
        else if(arg == "-u" || arg == "--unsafe")
        {
            PhysicsBasedAlgorithm::unsafe = true;
        }
        else if(arg == "-a" || arg == "--asap")
        {
            PhysicsBasedAlgorithm::asap = true;
        }
        else if(arg == "-g" || arg == "--graphical")
        {
            graphical = true;

            it = std::next(it);
            if(it != args.end())
            {
                auto str = *it;
                std::regex rgx("^\\d+x\\d+$");
                if(std::regex_match(str.begin(), str.end(), rgx))
                {
                    std::stringstream ss(str);
                    std::string buf;
                    std::getline(ss, buf, 'x');

                    width = std::stoi(buf);
                    std::getline(ss, buf);

                    height = std::stoi(buf);

                    if(width < 12U)
                        throw std::invalid_argument("Width must not be <12");
                    if(height < 12U)
                        throw std::invalid_argument("Height must not be <12");
                }
                else
                    it = std::prev(it);
            }
            else
                it = std::prev(it);
        }
        else
            throw std::invalid_argument("Unknown arg.");
    }
    if(goOn)
    {
        if(!loadFile && !heart)
        {
            if(secondExercise)
                std::cin >> ex2;
            else
                std::cin >> ex1;
        }
        else
        {
            if(heart)
            {
                makeHeart();
                filename = "examples/heart";
            }

            std::ifstream filebuf(filename);
            if(secondExercise)
                filebuf >> ex2;
            else
                filebuf >> ex1;
        }
    }
}

void Application::makeHeart() const noexcept
{
    std::vector<std::pair<float, float>> vertizes;
    vertizes.resize(heartPointCount);
    vertizes[0U] = std::make_pair(0.f, 1.f);

    const std::size_t halfCirclePointCount = (heartPointCount - 1U) / 2U;
    for(std::size_t i = 0U; i < halfCirclePointCount; ++i)
    {
        const float fac = (pi / halfCirclePointCount);

        const float x = .5f * std::cos(i * fac + pi) - .5f;
        const float y = .5f * std::sin(i * fac + pi) - .25f;
        vertizes[i + 1U] = std::make_pair(x, y);
    }

    for(std::size_t i = 0U; i < halfCirclePointCount; ++i)
    {
        const float fac = (pi / halfCirclePointCount);

        const float x = .5f * std::cos(i * fac + pi) + .5f;
        const float y = .5f * std::sin(i * fac + pi) - .25f;
        vertizes[i + halfCirclePointCount + 1U] = std::make_pair(x, y);
    }

    std::fstream of("examples/heart", std::ios::out);
    of << heartPointCount << " 1\n";
    for(const auto& v : vertizes)
        of << heartScale * v.first  << " "
           << heartScale * v.second << "\n";
    of << "\n0 0\n";
}

void Application::printHelp() const noexcept
{
    std::cout << "a1 -[h|s|f|g|he|hs|u|cf|mf|a]" << std::endl;
    std::cout << "Program normally solves part 1 of task 1." << std::endl;
    std::cout << std::endl << "Arguments:\n";
    std::cout << "-s --second\t\t\tDo the second task." << std::endl;
    std::cout << "-f --file\t\t\tLoad file" << std::endl;
    std::cout << "-g --graphical\t\t\tAdditonal, graphical output." << std::endl;
    std::cout << "-he --heart\t\t\tForce use of a heart graph." << std::endl;
    std::cout << "-hs --heart-scale\t\tScale of the heart. (>=10)" << std::endl;
    std::cout << "-u --unsafe\t\t\tAllow solutions with a rating below Luis one.";
    std::cout << std::endl;
    std::cout << "-cf --coulomb-factor\t\tSet the factor for the coulomb force.";
    std::cout << " (default is 1.0)" << std::endl;
    std::cout << "-mf --magnetic-force-factor\tSet the factor for the magnetic force.";
    std::cout << " (default is 128.0)" << std::endl;
    std::cout << "-a --asap\t\t\tFinish as quickly as possible.";
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "[-s --second] needs a point count, distance to the cake";
    std::cout << " and distance to each candle." << std::endl;
}
