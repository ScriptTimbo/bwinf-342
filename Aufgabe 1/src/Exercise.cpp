/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Exercise.hpp"
#include "Constants.hpp"
#include "CImg.hpp"

#include <algorithm>
#include <stdexcept>
#include <cassert>
#include <sstream>
#include <limits>
#include <cmath>

Exercise::Exercise() noexcept
         : points(), circles(), polygon()
{

}

Exercise::Exercise(const Polygon& pol, const PointList& poi) noexcept
         : points(poi), circles(), polygon(pol)
{
    for(const auto& p : points)
        circles.push_back({p, inf});
}

Exercise::Exercise(const PointList& pol, const PointList& poi) noexcept
         : points(poi), circles(), polygon(pol)
{
    //Polygon möglicherweise berichtigen
    if(polygon.polyArea() < 0.f)
        std::reverse(polygon.begin(), polygon.end());

    for(const auto& p : points)
        circles.push_back({p, inf});
}

void Exercise::visualizeCircles(std::size_t width,
                                std::size_t height,
                                std::string title) const noexcept
{
    const auto minMax = polygon.aabb();   
    const float scaleX = (( width - 10.f) / (minMax.B.first  - minMax.A.first ));
    const float scaleY = ((height - 10.f) / (minMax.B.second - minMax.A.second));

    using namespace cimg_library;
    CImg<unsigned char> img(width, height, 1, 3, 0);
    
    const float toAddX = (minMax.A.first  < 0 ? -minMax.A.first  : 0);
    const float toAddY = (minMax.A.second < 0 ? -minMax.A.second : 0);

    drawPolygon(img, scaleX, scaleY, toAddX, toAddY);
    drawPoints(img, scaleX, scaleY, toAddX, toAddY);

    CImgDisplay display(img, title.c_str());
    while(!(display.is_closed()))
        display.wait();

    img.save_png((title + " - output.png").c_str());
}

void Exercise::drawPolygon(cimg_library::CImg<unsigned char>& img,
                           float scaleX, float scaleY,
                           float toAddX, float toAddY) const noexcept
{
    constexpr const unsigned char polygonColor[] = { 102,  51,   0 };

    for(std::size_t i = 0U; i < polygon.size(); ++i)
    {
        const std::size_t j = ((i + 1U) % polygon.size());
        const auto& p0 = polygon[i];
        const auto& p1 = polygon[j];

        const float x0f = p0.first + toAddX;
        const auto x0  = static_cast<std::size_t>(x0f * scaleX) + 5U;

        const float y0f = p0.second + toAddY;
        const auto y0  = static_cast<std::size_t>(y0f * scaleY) + 5U;

        const float x1f = p1.first + toAddX;
        const auto x1  = static_cast<std::size_t>(x1f * scaleX) + 5U;

        const float y1f = p1.second + toAddY;
        const auto y1  = static_cast<std::size_t>(y1f * scaleY) + 5U;

        img.draw_line(x0, y0, 0, x1, y1, 0, polygonColor);

        for(int i = -1; i < 2; ++i)
        {
            for(int j = -1; j < 2; ++j)
            {
                for(std::size_t k = 0; k < 3; ++k)
                    img(x0 + i, y0 + j, 0, 0) = polygonColor[k];
            }
        }
    }
}

void Exercise::drawPoints(cimg_library::CImg<unsigned char>& img,
                          float scaleX, float scaleY,
                          float toAddX, float toAddY) const noexcept
{
    constexpr const std::size_t circlePointCount = 72U;
    constexpr const unsigned char pointsColor[]  = { 204,  0,   0 };
    constexpr const unsigned char circlesColor[] = { 153,  25,   0 };

    for(const auto& p : circles)
    {
        const float xf = p.center.first + toAddX;
        const auto x  = static_cast<std::size_t>(xf * scaleX) + 5U;
        const float yf = p.center.second + toAddY;
        const auto y  = static_cast<std::size_t>(yf * scaleY) + 5U;

        for(int i = -1; i < 2; ++i)
        {
            for(int j = -1; j < 2; ++j)
            {
                for(std::size_t k = 0; k < 3; ++k)
                    img(x + i, y + j, 0, k) = pointsColor[k];
            }
        }

        std::vector<Point> circlePoints;
        for(std::size_t i = 0U; i < circlePointCount; ++i)
        {
            const std::size_t angle = i * (360U / circlePointCount);
            const float rad = angle * (3.14159265358979323846264f / 180.f);
            
            const float cx = x + p.radius * scaleX * std::cos(rad);
            const float cy = y + p.radius * scaleY * std::sin(rad);

            circlePoints.emplace_back(cx, cy);

            if(0U < i)
            {
                const std::size_t j = i - 1U;
                
                const auto x0 = circlePoints[j].first;
                const auto y0 = circlePoints[j].second;
             
                const auto x1 = circlePoints[i].first;
                const auto y1 = circlePoints[i].second;   

                img.draw_line(x0, y0, 0, x1, y1, 0, circlesColor);
            }
        }
        const auto x0 = circlePoints[circlePointCount - 1U].first;
        const auto y0 = circlePoints[circlePointCount - 1U].second;
        const auto x1 = circlePoints[0U].first;
        const auto y1 = circlePoints[0U].second;

        img.draw_line(x0, y0, 0, x1, y1, 0, circlesColor);
    }
}

float Exercise::length(const Point& point) const noexcept
{
    return std::sqrt((point.first  * point.first)
                   + (point.second * point.second));
}

float Exercise::distance(const Point& point,
                         const LineSegment& line) const noexcept
{
    const float a = line.A.second - line.B.second;
    const float b = line.B.first - line.A.first;
    const float c = line.A.first * line.B.second
                  - line.B.first * line.A.second;

    const float xp = point.first;
    const float yp = point.second;

    //Lotfußpunkt
    const float X = (b * (b * xp - a * yp) - a * c)/(a * a + b * b);
    const float Y = (a * (a * yp - b * xp) - b * c)/(a * a + b * b);

    const float dist0  = LineSegment{Point(  X,   Y), line.B}.length();
    const float dist1  = LineSegment{Point(  X,   Y), line.A}.length();
    const float desdis = LineSegment{         line.A, line.B}.length();

    if(!(std::abs((dist0 + dist1) - desdis) < eps))
        return std::min(LineSegment{point, line.A}.length(),
                        LineSegment{point, line.B}.length());
    return LineSegment{point, {X, Y}}.length();
}

void Exercise::minRadiusOfPoints() noexcept
{
    const std::size_t pointCount = points.size();

    if(pointCount <= 1U)
        return;
    
    if(circles.empty())
        for(const auto& p : points)
            circles.push_back({p, inf});

    for(std::size_t i = 0U; i < pointCount; ++i)
    {
        const std::size_t k = ((i + 1U) % pointCount);
        float min = LineSegment{points[i], points[k]}.length();
        for(std::size_t j = 0; j < pointCount; ++j)
        {
            if(j == k || j == i)
                continue;
            const float val = LineSegment{points[i], points[j]}.length();
            
            if(val < min)
                min = val;
        }
        circles[i].radius = std::min(circles[i].radius, min / 2.f);
    }
}

void Exercise::minRadiusPoly() noexcept
{
    if(circles.empty())
        for(const auto& p : points)
            circles.push_back({p, inf});

    for(std::size_t j = 0U; j < points.size(); ++j)
    {
        const auto& p = points[j];
        float min = distance(p, LineSegment{polygon.back(),
                                            polygon.front()});
        for(std::size_t i = 0U; i < polygon.size(); ++i)
        {
            const std::size_t j = ((i + 1U) % polygon.size());
            const float dist = distance(p, LineSegment{polygon[i], polygon[j]});

            if(dist < min)
                min = dist;
        }
        circles[j].radius = std::min(circles[j].radius, min);
    } 
}

void Exercise::findRadii() noexcept
{
    minRadiusOfPoints();
    minRadiusPoly();
}
