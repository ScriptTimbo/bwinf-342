/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FirstExercise.hpp"
#include "Circle.hpp"

#include <algorithm>
#include <stdexcept>
#include <cassert>
#include <sstream>
#include <limits>
#include <cmath>

FirstExercise::FirstExercise() noexcept
              : Exercise()
{

}

FirstExercise::FirstExercise(const Polygon& pol,
                             const PointList& poi) noexcept
              : Exercise(pol, poi)
{

}

std::ostream& operator<<(std::ostream& os, FirstExercise& ex)
{
    return os << ex.ratio();
}

std::istream& operator>>(std::istream& is, FirstExercise& ex)
{
    std::size_t pointCount = 0U;
    std::size_t vertexCount = 0U;
    is >> vertexCount >> pointCount;
    is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    if(vertexCount < 3U)
        throw std::invalid_argument("A polygon has at least three points.");

    std::string line;
    std::getline(is, line);
    do
    {
        std::stringstream ss(line);

        std::string xs, ys;
        std::getline(ss, xs, ' ');
        std::getline(ss, ys);

        ex.polygon.vertices.push_back(Point{std::stof(xs), std::stof(ys)});
        std::getline(is, line);
    }
    while(!(line.empty()) && is.good());
    
    assert(ex.polygon.size() == vertexCount);
    
    if(ex.polygon.polyArea() < 0.f)
        std::reverse(ex.polygon.begin(), ex.polygon.end());

    std::getline(is, line);
    do
    {
        std::stringstream ss(line);
 
        std::string xs, ys;
        std::getline(ss, xs, ' ');
        std::getline(ss, ys);

        ex.points.push_back(Point{std::stof(xs), std::stof(ys)});
        std::getline(is, line);
    }
    while(!(line.empty()) && is.good());

    assert(ex.points.size() == pointCount);

    return is;
}

void FirstExercise::visualize(std::size_t width, std::size_t height) const noexcept
{
    visualizeCircles(width, height, "Lagebeziehung der Kerzen auf dem Kuchen.");
}

float FirstExercise::ratio() noexcept
{
    findRadii();

    const float pa = polygon.polyArea();

    float pc = 0.f;
    for(const auto& c : circles)
        pc += c.area();

    return (pc / pa);
}
