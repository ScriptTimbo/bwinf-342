/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Intersection.hpp"
#include "Constants.hpp"
#include <limits>
#include <cmath>

Intersection Intersection::intersect(LineSegment line, Circle circ) noexcept
{
    const auto dir = line.direction();
    const float t = dir.first  * (circ.center.first  - line.A.first)
                  + dir.second * (circ.center.second - line.A.second);

    const auto e = Point(dir.first  * t + line.A.first,
                         dir.second * t + line.A.second);
    const float dist = LineSegment{e, circ.center}.length();

    Intersection toReturn;
    if(dist - circ.radius < eps)
    {
        const float d = std::sqrt(circ.radius * circ.radius - dist * dist);

        toReturn.points.emplace_back((t - d) * dir.first  + line.A.first,
                                     (t - d) * dir.second + line.A.second);
        toReturn.points.emplace_back((t + d) * dir.first  + line.A.first,
                                     (t + d) * dir.second + line.A.second);
    }
    else if(std::abs(dist - circ.radius) <= eps)
        toReturn.points.push_back(e);
    return toReturn;
}

const bool Intersection::isTangent(Circle first, Circle second) noexcept
{
    const float addedRadii = first.radius + second.radius;
    const float distance = LineSegment{first.center, second.center}.length();

    return std::abs(addedRadii - distance) <= eps;
}
