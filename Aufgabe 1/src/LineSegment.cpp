/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LineSegment.hpp"
#include "Constants.hpp"

#include <cmath>

const float LineSegment::m() const noexcept
{
    return (B.second - A.second) / (B.first - A.first);
}

const float LineSegment::b() const noexcept
{
    return (A.second - m() * A.first);
}

const float LineSegment::length() const noexcept
{
    const float dX = B.first  - A.first;
    const float dY = B.second - A.second;

    return std::sqrt(dX * dX + dY * dY);
}

const Point LineSegment::direction() const noexcept
{
    const float len = length();

    return Point((B.first  - A.first)  / len,
                 (B.second - A.second) / len);
}

const bool LineSegment::isPointOnLine(const Point& p) const noexcept
{
    //X-Wert von p in Geradengleichung einsetzen
    const float newYP = std::abs(p.second - (m() * p.first + b()));

    if(std::abs(newYP - p.second) <= eps)
        return false;

    //Punkt liegt auf Gerade definiert durch A und B
    //Liegt er nun zwischen den Punkten ist er auf der Strecke

    if(std::abs(B.first - A.first) < std::abs(B.second - A.second)) // Hochkant
    {
        return (A.second > B.second
              ? (B.second <= p.second && p.second <= A.second)
              : (A.second <= p.second && p.second <= B.second));
    }
    return (A.first > B.first
          ? (B.first <= p.first && p.first <= A.first)
          : (A.first <= p.first && p.first <= B.first));
}
