/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PhysicsBasedAlgorithm.hpp"
#include "SecondExercise.hpp"
#include "FirstExercise.hpp"
#include "Constants.hpp"

#include <poly2tri/poly2tri.h>

#include <cassert>

bool PhysicsBasedAlgorithm::asap = false;
bool PhysicsBasedAlgorithm::unsafe = false;

float PhysicsBasedAlgorithm::magneticForceFactor = 128.f;
float PhysicsBasedAlgorithm::coulombFactor = 1.f;

PhysicsBasedAlgorithm::PhysicsBasedAlgorithm(
                        const Polygon& poly,
                        const PointList& initial) noexcept
                      : polygon(poly), points(initial), radius(inf),
                        impl(poly), triangulizedPoly(impl)
{
    FirstExercise ex(polygon, points);
    ex.findRadii();

    for(const auto& c : ex.circles)
        radius = std::min(radius, c.radius);
}

PointList PhysicsBasedAlgorithm::evaluate() noexcept
{
    ContactListener contactListener;

    const PointList luis = points;

    if(!asap)
    {
        letAttractToBarycenter();
        simulateCoulombForce();
    }

    FirstExercise ex0(polygon, luis);
    float result0 = ex0.ratio();

    std::size_t failCount = 0U;
    for(int k = 7; k < 1000; ++k)
    {
        const float toAdd = radius * (k / 1000.f);
        float r = radius + toAdd;
        
        bool isSleep = false;
        b2World world(b2Vec2(0.f, 0.f));
        world.SetContactListener(&contactListener);
        initWorld(world, r);
        for(std::size_t i = 0U; i < 60U; ++i)
        {
            world.Step((1.f/60.f), 256, 128);

            if((isSleep = isEveryoneAsleep(world)))
                break;
        }
        if(!isSleep)
            isSleep = isEveryoneAsleep(world);
        if(isSleep)
        {
            worldToPoints(world);

            if(asap)
                break;
        }
        else
            ++failCount;
        
        if(failCount > 50U)
            break;
    }

    FirstExercise ex1(polygon, points);
    float result1 = ex1.ratio();

    if(result0 < result1 || unsafe)
        return points;
    return luis;
}

void PhysicsBasedAlgorithm::simulateCoulombForce() noexcept
{
    const Point barycenter = polygon.barycenterOfPolygon();

    b2World world(b2Vec2(0.f, 0.f));
    initWorld(world, radius * 128.f);
    for(std::size_t i = 0U; i < 5U * 60U; ++i)
    {
        world.Step((1.f/60.f), 64, 32);
        for(b2Body* B = world.GetBodyList(); B; B = B->GetNext())
        {
            for(b2Body* b = B->GetNext(); b; b = b->GetNext())
            {
                //Jeder b2_dynamicBody ist ein Kreis
                //und jeder Kreis ist ein b2_dynamicBody
                if(B != b && B->GetType() == b2_dynamicBody)
                {
                    b2Vec2 direction = B->GetWorldCenter()
                                 - b->GetWorldCenter();
                    direction.Normalize();

                    const float fac = coulombFactor;
                    b->ApplyForce(-fac * direction, b->GetWorldCenter(), true);
                    B->ApplyForce(+fac * direction, B->GetWorldCenter(), true);
                }
            }
        }
    }

    worldToPoints(world);
}

void PhysicsBasedAlgorithm::letAttractToBarycenter() noexcept
{
    const Point barycenter = polygon.barycenterOfPolygon();

    b2World world(b2Vec2(0.f, 0.f));
    initWorld(world, radius * 128.f);
    for(std::size_t i = 0U; i < 20U * 60U; ++i)
    {
        world.Step((1.f/60.f), 64, 32);

        for(b2Body* b = world.GetBodyList(); b; b = b->GetNext())
        {
            //Jeder b2_dynamicBody ist ein Kreis
            //uns jeder Kreis ist ein b2_dynamicBody
            if(b->GetType() != b2_staticBody)
            {
                auto pos = b->GetPosition();

                const float fac = magneticForceFactor;
                b->ApplyLinearImpulse({(barycenter.first  - pos.x) * fac,
                                       (barycenter.second - pos.y) * fac},
                                      b2Vec2(), true);
            }
        }
    }
    worldToPoints(world);
}

void PhysicsBasedAlgorithm::initWorld(b2World& world, float radius) noexcept
{
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.fixedRotation = true;
    bodyDef.linearDamping = 0.01f;
    bodyDef.allowSleep = true;
    bodyDef.awake = true;

    //Hohe Genauigkeit
    bodyDef.bullet = true;

    b2CircleShape circleShape;
    circleShape.m_p.Set(0, 0);
    circleShape.m_radius = radius;

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circleShape;
    fixtureDef.density = 0.f;
    for(auto& p : points)
    {
        bodyDef.position.Set(p.first * 128.f, p.second * 128.f);

        b2Body* body = world.CreateBody(&bodyDef);
        body->CreateFixture(&fixtureDef);
    }

    bodyDef.type = b2_staticBody;
    bodyDef.position.Set(0.f, 0.f);
    b2Body* cakebody = world.CreateBody(&bodyDef);

    for(auto& t : triangulizedPoly)
    {
        b2Vec2 vec[3];
        for(std::size_t i = 0U; i < 3U; ++i)
        {
            const p2t::Point& p = *(t->GetPoint(i));
            vec[i].Set(p.x * 128.f, p.y * 128.f);
        }

        b2PolygonShape poly;
        poly.Set(vec, 3U);
        
        fixtureDef.shape = &poly;
        cakebody->CreateFixture(&fixtureDef);
    }
}

void PhysicsBasedAlgorithm::worldToPoints(b2World& world) noexcept
{
    auto pointsCpy = points;
    auto it = pointsCpy.begin();
    for(auto* p = world.GetBodyList(); p; p = p->GetNext())
    {
        if(p->GetType() != b2_dynamicBody)
            continue;
        auto pos = p->GetPosition();
        assert(it != pointsCpy.end());
        
        (*it) = Point{pos.x / 128.f, pos.y / 128.f};
        ++it;
    }
    assert(pointsCpy.size() == SecondExercise::pointCount);

    points = pointsCpy;
}

bool PhysicsBasedAlgorithm::isEveryoneAsleep(b2World& world) const noexcept
{
    bool isSleep = true;
    for(auto* p = world.GetBodyList(); p; p = p->GetNext())
    {
        if(p->IsAwake() && p->GetType() == b2_dynamicBody)
        {
            isSleep = false;
            break;
        }
    }
    return isSleep;
}

PhysicsBasedAlgorithm::TriangulizationImpl::TriangulizationImpl(
                                                const Polygon& poly) noexcept
        : cdt(), triangles(), polyline(), hole()
{
    const auto aabb = poly.aabb();
    const float width = std::abs(aabb.B.first - aabb.A.first);
    const float height = std::abs(aabb.B.second - aabb.A.second);
    polyline.push_back(new p2t::Point(aabb.A.first - width,
                                      aabb.A.second - height));

    polyline.push_back(new p2t::Point(aabb.A.first - width,
                                      aabb.B.second + height));

    polyline.push_back(new p2t::Point(aabb.B.first + width,
                                      aabb.B.second + height));

    polyline.push_back(new p2t::Point(aabb.B.first + width,
                                      aabb.A.second - height));

    hole.reserve(poly.size());
    for(auto& p : poly)
        hole.push_back(new p2t::Point(p.first, p.second));

    cdt = new p2t::CDT(polyline);
    cdt->AddHole(hole);
    cdt->Triangulate();

    triangles = cdt->GetTriangles();
}

PhysicsBasedAlgorithm::TriangulizationImpl::~TriangulizationImpl() noexcept
{
    for(auto& h : hole)
        delete h;
    for(auto& p : polyline)
        delete p;
    delete cdt;
}

PhysicsBasedAlgorithm::TriangulizationImpl::operator
  std::vector<p2t::Triangle*>&() noexcept
{
    return triangles;
}
