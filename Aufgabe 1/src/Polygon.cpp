/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Polygon.hpp"
#include "Constants.hpp"

Polygon::Polygon(const PointList& verts) noexcept
        : vertices(verts)
{

}

const float Polygon::polyArea() const noexcept
{   
    float sum = 0.f;
    for(std::size_t i = 0U; i < vertices.size(); ++i)
    {
        const std::size_t j = (i + 1U) % vertices.size();

        sum += ((vertices[i].second + vertices[j].second)
              * (vertices[i].first  - vertices[j].first));
    }

    return (sum / 2.f);
}

const Point Polygon::barycenterOfPolygon() const noexcept
{
    Point barycenter;
    for(std::size_t i = 0U; i < vertices.size(); ++i)
    {
        const std::size_t j = ((i + 1U) % vertices.size());
        const float factor = vertices[i].first * vertices[j].second
                           - vertices[j].first * vertices[i].second;

        barycenter.first  += (vertices[i].first  + vertices[j].first)  * factor;
        barycenter.second += (vertices[i].second + vertices[j].second) * factor;
    }
    const float A = polyArea();
    
    barycenter.first  = (1.f / (6.f * A)) * barycenter.first;
    barycenter.second = (1.f / (6.f * A)) * barycenter.second;

    return barycenter;
}

const Point Polygon::get(std::size_t i) const noexcept
{
    return vertices[i];
}

void Polygon::set(std::size_t i, const Point& p) noexcept
{
    vertices[i] = p;
}

Point& Polygon::operator[](std::size_t i) noexcept
{
    return vertices[i];
}

const Point& Polygon::operator[](std::size_t i) const noexcept
{
    return vertices[i];
}

PointList::iterator Polygon::begin() noexcept
{
    return vertices.begin();
}

PointList::const_iterator Polygon::begin() const noexcept
{
    return vertices.cbegin();
}

PointList::iterator Polygon::end() noexcept
{
    return vertices.end();
}

PointList::const_iterator Polygon::end() const noexcept
{
    return vertices.cend();
}

const Point Polygon::front() const noexcept
{
    return vertices.front();
}

const Point Polygon::back() const noexcept
{
    return vertices.back();
}

const std::size_t Polygon::size() const noexcept
{
    return vertices.size();
}

const bool Polygon::empty() const noexcept
{
    return vertices.empty();
}

const LineSegment Polygon::aabb() const noexcept
{ 
    float minX = inf;
    float minY = inf;

    float maxX = -inf;
    float maxY = -inf;

    for(const auto& p : vertices)
    {
        if(p.first < minX)
            minX = p.first;
        if(p.first > maxX)
            maxX = p.first;
        
        if(p.second < minY)
            minY = p.second;
        if(p.second > maxY)
            maxY = p.second;
    }

    return LineSegment{Point{minX, minY}, Point{maxX, maxY}};
}
