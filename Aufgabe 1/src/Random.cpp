/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Random.hpp"

#include <chrono>

Random::Random() noexcept
       : mt(std::chrono::steady_clock::now().time_since_epoch().count())
{

}

Random::operator std::mt19937&() noexcept
{
    return mt;
}

std::mt19937& Random::gen() noexcept
{
    return mt;
}
