/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SecondExercise.hpp"
#include "PhysicsBasedAlgorithm.hpp"
#include "FirstExercise.hpp"
#include "Triangulation.hpp"
#include "Intersection.hpp"
#include "Constants.hpp"
#include "Random.hpp"

#include <polyclipping/clipper.hpp>

#include <algorithm>
#include <stdexcept>
#include <cassert>
#include <sstream>
#include <limits>
#include <thread>

#include <fstream>

std::size_t SecondExercise::pointCount = 3U;

std::ostream& operator<<(std::ostream& os, SecondExercise& ex)
{
    os << "Luis: " << ex.luisRatio();
    return os << "\nLinda: " << ex.lindaRatio();
}

std::istream& operator>>(std::istream& is, SecondExercise& ex)
{
    std::size_t vertexCount = 0U;
    is >> vertexCount;
    is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    if(vertexCount < 3U)
        throw std::invalid_argument("A polygon has at least three points.");

    std::string line;
    std::getline(is, line);
    do
    {
        std::stringstream ss(line);

        std::string xs, ys;
        std::getline(ss, xs, ' ');
        std::getline(ss, ys);

        ex.polygon.vertices.push_back(Point{std::stof(xs), std::stof(ys)});
        std::getline(is, line);
    }
    while(!(line.empty()) && is.good());
    
    assert(ex.polygon.size() == vertexCount);

    if(ex.polygon.polyArea() < 0.f)
        std::reverse(ex.polygon.begin(), ex.polygon.end());
}

void SecondExercise::setLuisDistanceToCake(float dist) noexcept
{
    luisDistanceToCake = dist;
}

void SecondExercise::setLuisDistanceToCandle(float dist) noexcept
{
    luisDistanceToCandle = dist;
}

void SecondExercise::visualize(std::size_t width, std::size_t height) noexcept
{
    Exercise luis(polygon,
                  (luispoints.size() == pointCount
                  ? luispoints : PointList()));
    luis.findRadii();
    std::thread t0(&Exercise::visualizeCircles,
                   &luis, width, height,
                   "Lagebeziehung der Kerzen auf dem Kuchen - Luis");

    visualizeCircles(width, height,
                   "Lagebeziehung der Kerzen auf dem Kuchen - Linda");
    t0.join();
}

float SecondExercise::luisRatio() noexcept
{
    Polygon result = polygon;
    resizePoly(result, luisDistanceToCake);
    std::queue<Point> luisbuf;

    std::size_t specialTryCount = 0U;
    std::size_t totalTries = 0U;
    std::vector<Point> newResult;
    std::vector<Point> toInspect;
    while(luispoints.size() < pointCount && !result.empty()
       && totalTries < pointCount)
    {
        bool goOn = true;
        bool first = newResult.empty();
        do
        {
            if(first)
            {
                luispoints.push_back(result[0U]);
                if(doCirclesIntersect())
                {
                    if(!luisbuf.empty())
                        luisbuf.pop();
                    luispoints.pop_back();
                    break;
                }
                luisbuf.push(result[0U]);
                first = false;
            }
            else
            {
                const std::size_t old = luispoints.size();
                goOn = addLuisCircles(luisbuf, result, toInspect);

                if(!luisbuf.empty())
                {
                    if(old != luispoints.size())
                        newResult.push_back(luisbuf.front());
                    luisbuf.pop();
                }

                if(old != luispoints.size() && specialTryCount > 0U)
                {
                    //Falls der verwendete Punkt von luisbuf von toInspect kommt
                    
                    toInspect.pop_back();
                    --specialTryCount;
                }
            }
        } while(!luisbuf.empty() && luispoints.size() < pointCount);

        float perimeter = 0.f;
        for(std::size_t i = 0U; i < result.size(); ++i)
        {
            const std::size_t j = ((i + 1U) % result.size());
            perimeter += LineSegment{result[i], result[j]}.length();
        }
        const float delta = (perimeter - newResult.size() * diameter());
        const std::size_t oldSpecialTryCount = specialTryCount;

        if(luispoints.size() < pointCount
        && delta > luisDistanceToCandle
        && !toInspect.empty() && specialTryCount < toInspect.size())
        {
            // Es lassen sich bestimmt noch Kreise platzieren
            luisbuf.push(toInspect[specialTryCount]);

            ++specialTryCount;
            continue;
        }
        else
            specialTryCount = 0U;

        if(oldSpecialTryCount > specialTryCount)
            ++totalTries;

        if(luisbuf.empty() && luispoints.size() < pointCount)
        {
            totalTries = 0;
            if(!newResult.empty())
                result.vertices = newResult;
            newResult.clear();
            resizePoly(result, luisDistanceToCandle);
        }
    }

    if(luispoints.size() == pointCount)
        return FirstExercise(polygon, luispoints).ratio();
    return 0.f;
}

float SecondExercise::lindaRatio() noexcept
{
    if(luispoints.size() != pointCount)
    {
        //Luis Bewertung wird 0 sein,
        //jede zufällige Konfiguration ist besser
        Random rnd;
        points = getRandomPoints(rnd);
    }
    else
    {
        PhysicsBasedAlgorithm ph(polygon, luispoints);

        points = ph.evaluate();
    }
    findRadii();

    const float pa = polygon.polyArea();
    float pc = 0.f;

    for(const auto& c : circles)
        pc += c.area();

    return (pc / pa);
}

void SecondExercise::resizePoly(Polygon& newPoly, float dist)
{
    constexpr const float factor = 16384.f; // 2 ^ 14
    constexpr const float min = std::numeric_limits<int>::min() / factor;
    constexpr const float max = std::numeric_limits<int>::max() / factor;

    using namespace ClipperLib;
    Path subj;
    Paths solution;
    std::size_t index = 0U;
    for(const auto& p : newPoly)
    {
        const auto x = p.first * factor;
        const auto y = p.second * factor;

        if(p.first < min || p.second < min
        || p.first > max || p.second > max)
            throw std::logic_error("Points out of range. Please scale down!");

                            //kaufmännisch runden
        subj << IntPoint(x + (x < 0.f ? -.5f : .5f),
                         y + (y < 0.f ? -.5f : .5f));
    }
    ClipperOffset co;
    co.AddPath(subj, jtSquare, etClosedPolygon);
    co.Execute(solution, -dist * factor);

    if(solution.empty())
    {
        newPoly.vertices.clear();
        return;
    }

    const auto oldfirst = Point{newPoly[0U].first  * factor,
                                newPoly[0U].second * factor};
    newPoly.vertices.clear();
    newPoly.vertices.resize(solution.front().size());
    std::transform(solution[0U].begin(), solution[0U].end(), newPoly.begin(),
                    [factor](const IntPoint& ps)
                    { return std::make_pair(ps.X / factor, ps.Y / factor); });

    //Vorherige Ordnung wieder herstellen
    std::size_t ind = 0U;
    for(auto& p : solution[0U])
    {
        const float distToVertex = LineSegment{Point{p.X, p.Y},
                                               oldfirst}.length();
        if(std::abs(distToVertex - dist * factor) <= factor)
            break;
        ++ind;
    }
    std::rotate(newPoly.begin(), std::next(newPoly.begin(),ind), newPoly.end());
}

bool SecondExercise::doCirclesIntersect() const noexcept
{
    const float diametre = diameter();
    for(std::size_t i = 0U; i < luispoints.size(); ++i)
    {
        for(std::size_t j = i + 1U; j < luispoints.size(); ++j)
        {
            const float dist = LineSegment{luispoints[i],
                                           luispoints[j]}.length();

            //Abstand kleiner Durchmesser -> Kollision
            if(dist - diametre <= -eps)
                return true;
        }
    }
    return false;
}

float SecondExercise::diameter() const noexcept
{
    return (luisDistanceToCandle < luisDistanceToCake * 2.f
          ? 2.f * luisDistanceToCake
           : luisDistanceToCandle);
}

bool SecondExercise::addLuisCircles(std::queue<Point>& luisbuf,
                           Polygon& curpoly, PointList& toInspect) noexcept
{
    const auto relativePoint = luisbuf.front();

    std::size_t count = 0U;
    const float diametre = diameter();
    Circle relativeCircle{relativePoint, diametre};

    for(std::size_t i = 0U; i < curpoly.size(); ++i)
    {
        const std::size_t j = ((i + 1U) % curpoly.size());

        //Kollisionsprüfung mit doppeltem Radius
        relativeCircle.radius = diametre;
        Intersection inter = Intersection::intersect({curpoly[i], curpoly[j]},
                                                     relativeCircle);
        relativeCircle.radius = diametre / 2.f;
        for(auto& p : inter.points)
        {
            if(!LineSegment{curpoly[i], curpoly[j]}.isPointOnLine(p)
            || !Intersection::isTangent({p, diametre / 2.f}, relativeCircle))
                continue;

            luispoints.push_back(p);
            if(doCirclesIntersect())
            {
                luispoints.pop_back();
                continue;
            }

            if(luispoints.size() >= pointCount)
                return true;

            if((count++) == 0U)
                luisbuf.push(p);
            else
                toInspect.push_back(p);
        }
    }

    //Wurde auch nur ein Kreis gesetzt, war es erfolgreich
    //(count ist ein unsigned)
    return count != 0U;
}

PointList SecondExercise::getRandomPoints(Random& rnd) const noexcept
{
    PointList points;
    points.resize(pointCount);
    Triangulation tri(polygon);
    std::vector<Polygon> triangles = tri;

    for(auto& p : points)
        p = randomPointInPolygon(rnd.gen(), triangles);
    return points;
}

Point SecondExercise::randomPointInPolygon(std::mt19937& mersenneTwister,
               const std::vector<Polygon>& triangulizedPolygon) const noexcept
{
    const auto tris = triangleIndizes(triangulizedPolygon);

    std::uniform_real_distribution<float> dist(0.f, 1.f);
    const float percentage = dist(mersenneTwister);
    const std::size_t index = tris.lower_bound(percentage)->second;

    const auto& triangle = triangulizedPolygon[index];
    const auto A = triangle[0U];
    const auto B = triangle[1U];
    const auto C = triangle[2U];

    //Umsetzung der Formel für einen zufälligen Punkt im Dreieck aus
    //"Shape Distributions" von B. C. d. D. D. Robert Osada und Thomas Funkhouser
    const float r1 = dist(mersenneTwister);
    const float r2 = dist(mersenneTwister);

    const float sqr1 = std::sqrt(r1);

    const auto tA = Point((1.f - sqr1) * A.first,
                          (1.f - sqr1) * A.second);
    const auto tB = Point(sqr1 * (1.f - r2) * B.first,
                          sqr1 * (1.f - r2) * B.second);
    const auto tC = Point(sqr1 * r2 * C.first,
                          sqr1 * r2 * C.second);

    return Point(tA.first  + tB.first  + tC.first,
                 tA.second + tB.second + tC.second);
}

std::map<float, std::size_t> SecondExercise::triangleIndizes(
               const std::vector<Polygon>& triangulizedPolygon) const noexcept
{
    std::map<float, std::size_t> m;

    const float A = polygon.polyArea();
    float total = 0.f; //> Summe der letzten Wahrscheinlichkeiten
    for(std::size_t i = 0U; i < triangulizedPolygon.size(); ++i)
    {
        const auto& t = triangulizedPolygon[i];

        const float a = LineSegment{t[0U], t[1U]}.length();
        const float b = LineSegment{t[1U], t[2U]}.length();
        const float c = LineSegment{t[2U], t[0U]}.length();
        const float s = (a + b + c) / 2.f;

        float F = std::sqrt(s * (s - a) * (s - b) * (s - c));
        total += F;

        m[total / A] = i;
    }

    return m;
}
