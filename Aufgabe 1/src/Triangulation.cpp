/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Triangulation.hpp"
#include "Constants.hpp"

#include <algorithm>
#include <limits>

Triangulation::Triangulation(const Polygon& polygon) noexcept
              : poly(polygon)
{

}

Triangulation::operator std::vector<Polygon>() noexcept
{
    std::vector<Polygon> triangulizedPolygon;
    process(poly, triangulizedPolygon);
    return triangulizedPolygon;
}

bool Triangulation::process(const Polygon& poly,
                            std::vector<Polygon>& triangulizedPoly) noexcept
{
    const std::size_t n = poly.size();

    //Nicht triangulierbar
    if(n < 3U)
        return false;

    std::vector<std::size_t> V;
    V.resize(n);

    //Richtung im Buffer bestimmen
    const float area = poly.polyArea();
    for(std::size_t vertex = 0U; vertex < n; ++vertex)
    {
        if(0.f < area)
            V[vertex] = vertex;
        else
            V[vertex] = (n - 1) - vertex;
    }

    std::size_t nv = n;
    int count = 2 * nv;

    for(std::size_t i = 0U, v= nv - 1U; 2U < nv; )
    {
        if((count--) <= 0U)
            return false;

        std::size_t u = v;
        if(nv <= u)
            u = 0U;

        v = u + 1U;
        if(nv <= v)
            v = 0U;

        std::size_t w = v + 1U;
        if(nv <= w)
            w = 0U;

        //Prüfen, ob das "Ohr" abschneidbar ist
        if(snip(poly, u, v, w, nv, V))
        {
            const std::size_t a = V[u];
            const std::size_t b = V[v];
            const std::size_t c = V[w];

            triangulizedPoly.emplace_back(PointList{poly[a], poly[b], poly[c]});

            ++i;

            //Buffer aktualisieren
            for(std::size_t s = v, t = v + 1U; t < nv; ++s,++t)
                V[s] = V[t];
            --nv;

            count = 2 * nv;
        }
    }

    return true;
}

bool Triangulation::snip(const Polygon& poly,
                    std::size_t u, std::size_t v, std::size_t w,
                    std::size_t nv, std::vector<std::size_t>& V) noexcept
{
    const auto A = poly[V[u]];
    const auto B = poly[V[v]];
    const auto C = poly[V[w]];

    const Point ba(B.first - A.first, B.second - A.second);
    const Point ca(C.first - A.first, C.second - A.second);

    if(eps > (ba.first * ca.second) - (ba.second * ca.first))
        return false;

    for(std::size_t p = 0U; p < nv; ++p)
    {
        if((p == u) || (p == v) || (p == w))
            continue;

        //Liegt ein Punkt im "Ohr", ist es kein "Ohr"
        if(inTriangle(A, B, C, poly[V[p]]))
            return false;
    }

    return true;
}

bool Triangulation::inTriangle(Point A, Point B, Point C, Point P) noexcept
{
    const auto _A = std::make_pair(C.first - B.first, C.second - B.second);
    const auto _B = std::make_pair(A.first - C.first, A.second - C.second);
    const auto _C = std::make_pair(B.first - A.first, B.second - A.second);

    const auto PA = std::make_pair(P.first - A.first, P.second - A.second);
    const auto PB = std::make_pair(P.first - B.first, P.second - B.second);
    const auto PC = std::make_pair(P.first - C.first, P.second - C.second);

    const auto axbp = _A.first * PB.second - _A.second * PB.first;
    const auto cxap = _C.first * PA.second - _C.second * PA.first;
    const auto bxcp = _B.first * PC.second - _B.second * PC.first;

    //Punkt im Dreieck, wenn diese Ungleichungen erfüllt sind
    return ((axbp >= 0.f) && (bxcp >= 0.f) && (cxap >= 0.f));
}
