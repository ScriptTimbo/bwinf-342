#!/bin/bash

source="$(find src/*.cpp | tr '\n' ' ')"
link="-I include/"

function debug
{
    mkdir -p bin
    mkdir -p bin/Debug
    g++ -ggdb -std=c++14 $source $link

    if [ -f a.out ]
    then
        mv a.out bin/Debug/a3
    fi
}

function release
{
    mkdir -p bin
    mkdir -p bin/Release
    g++ -O2 -s -std=c++14 $source $link

    if [ -f a.out ]
    then
        mv a.out bin/Release/a3
    fi
}

dr=$1
if [ -z "$dr" ] ; then
    read -p "Compile for [Dd]ebug or [Rr]elease? (q to quit) : " dr
fi
while true; do
    case $dr in
    [Dd]* ) debug; break;;
    [Rr]* ) release; break;;
     [q]* ) exit;;
        * ) echo "Please provide an valid answer, either d or r!";;
    esac
    read -p "Compile for [Dd]ebug or [Rr]elease? (q to quit) : " dr
done
