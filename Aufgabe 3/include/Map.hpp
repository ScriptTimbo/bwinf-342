/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "Tile.hpp"

#include <ostream>
#include <string>
#include <vector>

class Map
{
private:
    enum class CycleAttr
    {
        MightBeSafe,
        DeadEnd,
        Invalid,
    };
public:
    enum class CycleProcStrat
    {
        Basic,
        Advanced,
    };
public:
    using CycleIterator = std::vector<std::pair<std::vector<Tile*>,
                                                CycleAttr>>::iterator;
    using DirT = std::pair<int, Direction>;
    using DirIt = std::vector<DirT>::iterator;
public:
	explicit Map(std::string file);

    friend std::ostream& operator<<(std::ostream& os, Map& m) noexcept;
private:
    std::vector<bool> getSafeTiles() noexcept;

    std::size_t w() const noexcept;
    std::size_t h() const noexcept;

    void isSafe(std::vector<Tile*>& buf, DirT from,
                std::vector<bool>& safe) noexcept;
    std::vector<Tile*> findNodes() noexcept;
    bool hasAdjacentWalls(Tile& t) const noexcept;
    bool hasOppositePathways(Tile& t, Direction from) const noexcept;
    bool isOnPossiblePathway(Tile& t, std::pair<Tile*, Tile*>& pair) noexcept;
    bool pathWithExit(Tile& prev, Tile& cur, DirT dir) noexcept;
    void precomputeMovement() noexcept;
    void precomputeCornerGraph() noexcept;

    CycleAttr processCycle(std::vector<Tile*>& v,
                           CycleProcStrat strat
                                = CycleProcStrat::Advanced) noexcept;
    CycleIterator findCycle(Tile& t) noexcept;
private:
    std::vector<Tile> tiles;
    int width;
    int height;

    std::vector<std::pair<int, Direction>> directions;
    std::vector<std::pair<std::vector<Tile*>, CycleAttr>> cycles;
    std::vector<Tile*> graph;
};
