/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "Direction.hpp"

#include <cstddef>
#include <map>

class Tile
{
public:
    enum class Type
    {
        Wall,
        Space,
        Exit,
    };
public:
    explicit Tile(std::size_t x, std::size_t y, Type type) noexcept;

    std::size_t x() const noexcept;
    std::size_t y() const noexcept;
    Type type() const noexcept;

    void addReachableTiles(Tile& t, Direction dir, Tile& op) noexcept;
    bool hasReachableTile(Direction) noexcept;

    Tile& get(Direction dir) noexcept;
private:
    std::size_t X;
    std::size_t Y;
    Type myType;

    std::map<Direction, Tile*> map;
};
