/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Application.hpp"

#include <iostream>
#include <fstream>

Application::Application(const std::vector<std::string>& arguments) noexcept
            : goOn(true), map( readArguments(arguments) )
{

}

int Application::run() noexcept
{
    if(!goOn)
        return 0;
    std::cout << map << std::endl;

    return 0;
}

std::string Application::readArguments(const std::vector<std::string>& args)
{
    std::string filename;
    bool loadFile = false;
    for(auto it = args.begin(); it != args.end(); ++it)
    {
        const auto arg = *it;

        if(arg == "-h" || arg == "--help")
        {
            printHelp();
            goOn = false;
        }
        else if(arg == "-f" || arg == "--file")
        {
            it = std::next(it);
            if(it == args.end())
                throw std::invalid_argument("No file name was given.");

            loadFile = true;
            filename = *it;
        }
        else
            throw std::invalid_argument("Unknown arg.");
    }
    if(goOn)
    {
        if(!loadFile)
        {
            std::cout << "No file was given." << std::endl;
            
            goOn = false;
        }
        else
            return filename;
    }

    return "";
}

void Application::printHelp() const noexcept
{
    std::cout << "a3 -[hf]" << std::endl;
    std::cout << std::endl << "Arguments:\n";
    std::cout << "-h --help\tPrint this help" << std::endl;
    std::cout << "-f --file\tLoad file" << std::endl;
}
