/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Map.hpp"

#include <algorithm>
#include <stdexcept>
#include <cassert>
#include <fstream>
#include <queue>

Map::Map(std::string file) 
    : tiles(), width(), height(),
      directions(), cycles(),
      graph()
{
    if(file.empty())
      return;

    std::ifstream filestream(file);

    if(!(filestream))
        throw std::invalid_argument(file + " not accessible.");
    
    std::string buf;
    while(std::getline(filestream, buf))
    {
        if(width == 0U)
            width = buf.size() - (buf.find('\r') != std::string::npos ? 1U:0U);

        for(std::size_t i = 0U; i < buf.size(); ++i)
        {
            Tile::Type toAdd = Tile::Type::Wall;

            if(buf[i] == '#')
                toAdd = Tile::Type::Wall;
            else if(buf[i] == ' ')
                toAdd = Tile::Type::Space;
            else if(buf[i] == 'E')
                toAdd = Tile::Type::Exit;
            else if(buf[i] == '\r')
                continue;
            else
                throw std::logic_error("File has an unsupported format.");

            const std::size_t x = i;
            const std::size_t y = height;

            tiles.emplace_back(x, y, toAdd);
        }

        ++height;
    }

    directions = { { width, Direction::Down   }, { 1 , Direction::Right  },
                   {-width, Direction::Up     }, {-1 , Direction::Left   } };

    graph = findNodes();
    precomputeMovement();
    precomputeCornerGraph();
}

std::ostream& operator<<(std::ostream& os, Map& m) noexcept
{
    const auto saveMap = m.getSafeTiles();
    for(std::size_t y = 0U; y < m.h(); ++y)
    {
        for(std::size_t x = 0U; x < m.w(); ++x)
        {
            auto& toPrint = m.tiles[x + y * m.w()];

            if(toPrint.type() == Tile::Type::Wall)
                os << "#";
            else if(toPrint.type() == Tile::Type::Exit)
                os << "E";
            else if(toPrint.type() == Tile::Type::Space)
            {
                if(saveMap[x + y * m.w()])
                    os << "S";
                else
                    os << " ";
            }
        }

        if(y + 1U < m.h())
           os << std::endl;
    }

    return os;
}

std::vector<bool> Map::getSafeTiles() noexcept
{
    std::vector<bool> safeTiles;
    safeTiles.resize(width * height, true);

    bool noDeadEnd = true;
    for(auto& c : cycles)
    {
        if(c.second == CycleAttr::DeadEnd)
            noDeadEnd = false;
    }

    if(noDeadEnd)
        return safeTiles;

    for(auto& t : tiles)
    {
        const std::size_t x = t.x();
        const std::size_t y = t.y();

        if(t.type() == Tile::Type::Wall)
            continue;

        std::vector<Tile*> buf;
        buf.push_back(&t);
        isSafe(buf, directions[0], safeTiles);
    }
    return safeTiles;
}

std::size_t Map::w() const noexcept
{
    return width;
}

std::size_t Map::h() const noexcept
{
    return height;
}

void Map::isSafe(std::vector<Tile*>& buf, Map::DirT from,
                 std::vector<bool>& safe) noexcept
{
    auto& cur = *(buf.back());
    const std::size_t index = cur.x() + cur.y() * width;
    if(!safe[index])
        return;

    if(buf.size() > 1U)
    {
        auto& prev = *(*(std::prev(buf.end(), 2)));

        if(pathWithExit(prev, cur, from))
            return;
    }
    else
    {
        if(cur.type() == Tile::Type::Exit)
            return;
    }

    auto cycle = findCycle(cur);
    if(cycle != cycles.end() && (cycle->second == CycleAttr::DeadEnd))
    {
        safe[index] = false;
        return;
    }

    for(auto d : directions)
    {
        if(!cur.hasReachableTile(d.second))
            continue;
        auto& next = cur.get(d.second);

        //Bereits besuchte Felder ausschließen
        if(std::find(buf.begin(), buf.end(), &next) != buf.end())
            continue;

        if(!safe[next.x() + next.y() * width])
        {
            if(!pathWithExit(cur, next, d))
            {
                safe[index] = false;
                return;
            }
        }

        buf.push_back(&next);
        isSafe(buf, d, safe);

        if(!safe[next.x() + next.y() * width])
        {
            if(!pathWithExit(cur, next, d))
            {
                safe[index] = false;
                return;
            }
        }

        buf.pop_back();
    }
}

std::vector<Tile*> Map::findNodes() noexcept
{
    std::vector<Tile*> nodes;
    for(auto& t : tiles)
    {
        if(t.type() != Tile::Type::Wall
        && hasAdjacentWalls(t))
        {
            nodes.push_back(&t);
        }
    }

    return nodes;
}

bool Map::hasAdjacentWalls(Tile& t) const noexcept
{
    const std::size_t x = t.x();
    const std::size_t y = t.y();
    const std::size_t index = x + y * width;

    const std::size_t i0 = index + directions[0].first;
    const std::size_t i1 = index + directions[1].first;
    const std::size_t i2 = index + directions[2].first;
    const std::size_t i3 = index + directions[3].first;

    const bool b0 = (i0 < tiles.size() && tiles[i0].type() == Tile::Type::Wall);
    const bool b1 = (i1 < tiles.size() && tiles[i1].type() == Tile::Type::Wall);
    const bool b2 = (i2 < tiles.size() && tiles[i2].type() == Tile::Type::Wall);
    const bool b3 = (i3 < tiles.size() && tiles[i3].type() == Tile::Type::Wall);

    //Alle möglichen Wandpositionen durchlaufen
    //// naiv, unschön, aber geht (-:
    return ((( b0 &&  b1 && !b2 &&  b3)
          || ( b0 &&  b1 &&  b2 && !b3)
          || ( b0 &&  b1 && !b2 && !b3))
         || (( b0 && !b1 &&  b2 &&  b3)
          || ( b0 &&  b1 && !b2 &&  b3)
          || ( b0 && !b1 && !b2 &&  b3))
         || ((!b0 &&  b1 &&  b2 &&  b3)
          || ( b0 && !b1 &&  b2 &&  b3)
          || (!b0 && !b1 &&  b2 &&  b3))
         || (( b0 &&  b1 &&  b2 && !b3)
          || (!b0 &&  b1 &&  b2 &&  b3)
          || (!b0 &&  b1 &&  b2 && !b3))
         || (( b0 &&  b1 &&  b2 &&  b3)));
}

bool Map::hasOppositePathways(Tile& t, Direction from) const noexcept
{
    bool horizontal = (from == Direction::Left || from == Direction::Right);

    if(horizontal)
    {
        return (t.hasReachableTile(Direction::Up)
             && t.hasReachableTile(Direction::Down));
    }
    return (t.hasReachableTile(Direction::Left)
         && t.hasReachableTile(Direction::Right));
}

void Map::precomputeMovement() noexcept
{
    for(std::size_t i = 0U; i < tiles.size(); ++i)
    {
        if(tiles[i].type() == Tile::Type::Wall)
            continue;

        for(std::size_t j = 0U; j < 2U; ++j)
        {
            const auto d = directions[j];
            const auto p = directions[j + 2U];

            //Nichts doppelt hinzufügen
            if(tiles[i].hasReachableTile(d.second))
                continue;

            int next = i + d.first;
            int last = i;
            while(next < tiles.size()
            && tiles[last].type() != Tile::Type::Wall)
            {
                //bis zu einer Wand "laufen"
                if(tiles[next].type() == Tile::Type::Wall)
                {
                    if(i != last)
                    {
                        //Haben wir uns überhaupt ein Feld vorbewegt,
                        //fügen wir diese Xi-Kante nun den beiden Feldern hinzu,
                        //bei denen wir angefangen hatten

                        tiles[i].addReachableTiles(tiles[last],
                                                   d.second, tiles[i]);

                        tiles[last].addReachableTiles(tiles[i],
                                                   p.second, tiles[last]);
                    }

                    int n = i + d.first;
                    while(n != next)
                    {
                        //Jedem Feld des Weges, den wir langliefen, eine
                        //Xi-Kante hinzufügen
                        tiles[n].addReachableTiles(tiles[last],
                                                   d.second, tiles[i]);

                        n = n + d.first;
                    }
                }

                last = next;
                next = next + d.first;
            }
        }
    }
}

void Map::precomputeCornerGraph() noexcept
{
    CycleProcStrat strat = CycleProcStrat::Basic;
    bool advancedCycleHere = false;
    do
    {
        advancedCycleHere = false;
        for(const auto& t : graph)
        {
            if(findCycle(*t) != cycles.end() || !hasAdjacentWalls(*t))
                continue;
            cycles.push_back(std::make_pair(std::vector<Tile*>(),
                                            CycleAttr::MightBeSafe));
            auto& cycle = cycles.back();
            
            std::vector<Tile*> v;
            v.push_back(t);
            cycle.second = processCycle(v, strat);

            if(cycle.second == CycleAttr::Invalid)
            {
                //Leider fehlt uns Information, um *diesen*
                //Zyklus zu bestimmen

                cycles.pop_back();
                advancedCycleHere = true;
            }
            else
            {
                cycle.first = v;
            }
        }
        strat = CycleProcStrat::Advanced;
    } while(advancedCycleHere);
}

Map::CycleAttr Map::processCycle(std::vector<Tile*>& v,
                                 Map::CycleProcStrat strat) noexcept
{
    bool hasExit = false;
    std::queue<Tile*> buf;
    buf.push(v.back());

    while(!buf.empty())
    {
        Tile& tilebuf = *(buf.front());
        hasExit = (hasExit ? hasExit : (tilebuf.type() == Tile::Type::Exit));

        for(auto d : directions)
        {
            if(!tilebuf.hasReachableTile(d.second))
                continue;

            auto& reachable = tilebuf.get(d.second);
            if(pathWithExit(tilebuf, reachable, d))
            {
                v.push_back(&reachable);
                hasExit = true;
                continue;
            }

            //Das Wissen von schon bestehenden Zyklen nutzen
            auto cit = findCycle(reachable);
            if(cit != cycles.end())
            {
                if(cit->second == CycleAttr::MightBeSafe)
                    hasExit = true;
                continue;
            }
            //Nichts doppelt hinzufügen
            auto vit = std::find(v.begin(), v.end(), &reachable);
            if(vit != v.end())
                continue;

            v.push_back(&reachable);

            //"Normale" Strategie
            if(strat == CycleProcStrat::Advanced)
            {
                std::pair<Tile*, Tile*> pair;
                auto git0 = std::find(graph.begin(), graph.end(), &tilebuf);
                auto git1 = std::find(graph.begin(), graph.end(), &reachable);
                if(git0 == graph.end() && git1 == graph.end()
                && isOnPossiblePathway(reachable, pair))
                {
                    bool cont = false;
                    cit = findCycle(*pair.first);
                    if(cit != cycles.end()
                    && cit->second == CycleAttr::MightBeSafe)
                    {
                        hasExit = true;
                        cont = true;
                    }

                    cit = findCycle(*pair.second);
                    if(cit != cycles.end()
                    && cit->second == CycleAttr::MightBeSafe)
                    {
                        hasExit = true;
                        cont = true;
                    }

                    vit = std::find(v.begin(), v.end(), pair.first);
                    if(vit == v.end())
                        cont = true;

                    vit = std::find(v.begin(), v.end(), pair.second);
                    if(vit == v.end())
                        cont = true;

                    if(cont)
                        continue;
                }
            }
            else
            {
                if(!hasAdjacentWalls(reachable))
                {
                    bool invalid = false;
                    std::pair<Tile*, Tile*> pair;
                    if(isOnPossiblePathway(reachable, pair))
                    {
                        vit = std::find(v.begin(), v.end(), pair.first);
                        bool isKnown = (vit != v.end());

                        vit = std::find(v.begin(), v.end(), pair.second);
                        if(vit != v.end() || isKnown)
                            invalid = false;
                        else
                            invalid = true;
                    }
                    else
                        invalid = true;
                    if(invalid)
                    {
                        v.clear();
                        return CycleAttr::Invalid;
                    }
                }
            }

            buf.push(&reachable);
        }
        buf.pop();
    }   

    return (hasExit ? CycleAttr::MightBeSafe : CycleAttr::DeadEnd);
}

bool Map::pathWithExit(Tile& prev, Tile& cur, DirT dir) noexcept
{
    assert(&prev != &cur);
    assert(&prev.get(dir.second) == &cur);

    if(cur.type() == Tile::Type::Exit)
        return true;
    if(prev.type() == Tile::Type::Exit)
        return true;

    bool hasExit = false;
    auto* t = &prev;
    while(t != &cur)
    {
        //Zwischen den gegebenen Feldern entlanglaufen und 
        //nach Ausgängen suchen 
        
        if(t->type() == Tile::Type::Exit)
        {
            hasExit = true;
            break;
        }
        else if(t->type() == Tile::Type::Wall)
            break;

        const std::size_t index = t->x() + t->y() * width;
        const std::size_t next =  index + dir.first;

        assert(next < tiles.size());

        auto* ct = t;
        t = &tiles[next];

        if(ct == t)
            break;
    }

    return hasExit;
}

bool Map::isOnPossiblePathway(Tile& t, std::pair<Tile*, Tile*>& pair) noexcept
{
    bool isBetweenTwoNodes = false;

    std::vector<Tile*> buf = graph;
    while(!buf.empty())
    {
        auto& tilebuf = *buf.front();
        for(auto d : directions)
        {
            if(!tilebuf.hasReachableTile(d.second))
                continue;

            auto& reachable = tilebuf.get(d.second);
            auto git = std::find(graph.begin(), graph.end(), &reachable);
            if(git == graph.end())
                continue;
            
            auto* ti = buf.front();
            while(ti != &reachable)
            {
                if(ti == &t)
                {
                    pair = std::make_pair(buf.front(), &reachable);
                    isBetweenTwoNodes = true;
                    break;
                }
                const std::size_t index = ti->x() + ti->y() * width;
                const std::size_t next = index + d.first;

                assert(next < tiles.size());
                ti = &tiles[next];
            }
            if(!isBetweenTwoNodes)
                if((isBetweenTwoNodes = (ti == &t)))
                    pair = std::make_pair(ti, &t);

            if(isBetweenTwoNodes)
                break;
        }
        buf.erase(buf.begin());
        if(isBetweenTwoNodes)
            break;
    }
    return isBetweenTwoNodes;
}

Map::CycleIterator Map::findCycle(Tile& t) noexcept
{
    using T = std::pair<std::vector<Tile*>, CycleAttr>;

    return std::find_if(cycles.begin(), cycles.end(),
                        [&t](const T& t0)
                        {
                            return std::find(t0.first.begin(), t0.first.end(),
                                             &t) != t0.first.end();
                        });
}
