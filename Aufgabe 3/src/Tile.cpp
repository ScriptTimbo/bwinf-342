/*
	Copyright (C) 2016 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Tile.hpp"

#include <cassert>

Tile::Tile(std::size_t x, std::size_t y, Tile::Type type) noexcept
     : X(x), Y(y), myType(type), map()
{

}

std::size_t Tile::x() const noexcept
{
    return X;
}

std::size_t Tile::y() const noexcept
{
    return Y;
}

Tile::Type Tile::type() const noexcept
{
    return myType;
}

void Tile::addReachableTiles(Tile& t, Direction dir, Tile& op) noexcept
{
    map[dir] = &t;

    Direction opposite = dir;
    if(opposite == Direction::Up)
        opposite = Direction::Down;
    else if(opposite == Direction::Down)
        opposite = Direction::Up;
    else if(opposite == Direction::Left)
        opposite = Direction::Right;
    else if(opposite == Direction::Right)
        opposite = Direction::Left;

    map[opposite] = &op;
}

bool Tile::hasReachableTile(Direction dir) noexcept
{
    auto it = map.find(dir);

    return it != map.end() && it->second != this;
}

Tile& Tile::get(Direction dir) noexcept
{
    assert(map[dir]);

    return *map[dir];
}
